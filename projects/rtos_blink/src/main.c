/*
 ******************************************************************************
 * @file    main.c
 * @author  Ac6
 * @version V1.0
 * @date    01-December-2013
 * @brief   Default main function.
 ******************************************************************************
 */

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"

extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,signed portCHAR *pcTaskName);

void
vApplicationStackOverflowHook(xTaskHandle *pxTask,signed portCHAR *pcTaskName) {
    (void)pxTask;
    (void)pcTaskName;
    for(;;);
}

void taskLedBlink(void *taskParmPtr);

BitAction led = 0;

int main(void) {

    RCC->APB2ENR |= (RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPCEN | RCC_APB1ENR_TIM2EN);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC, ENABLE);
    // initialize GPIO structure
    GPIO_InitTypeDef gpioInitStruct;
    GPIO_StructInit(&gpioInitStruct);
    gpioInitStruct.GPIO_Pin = GPIO_Pin_13;
    gpioInitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    gpioInitStruct.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOC, &gpioInitStruct);


    xTaskCreate(taskLedBlink,                 // FUNCTION
                (const char *)"taskLedBlink", // TASK NAME
                configMINIMAL_STACK_SIZE * 1,       // TASK STACK
                NULL,                               // TASK ARGS
                tskIDLE_PRIORITY + 1,               // TASK PRIORITY
                NULL                                // SYSTEM TASK POINTER
                );

    vTaskStartScheduler();

    while(1){
    }

}


void taskLedBlink(void *taskParmPtr) {
    while(1){
        GPIO_WriteBit(GPIOC, GPIO_Pin_13, 1);
    }
}

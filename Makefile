## Local functions
define logger-compile
	@printf "%6s\t%-30s\n" $(1) $(2)
endef	

## ARM NONE EABI
ARM_PREFIX	?= arm-none-eabi
CC					:= $(ARM_PREFIX)-gcc
CXX					:= $(ARM_PREFIX)-g++
LD					:= $(ARM_PREFIX)-gcc
AR					:= $(ARM_PREFIX)-ar
AS					:= $(ARM_PREFIX)-as
OBJCOPY			:= $(ARM_PREFIX)-objcopy
SIZE				:= $(ARM_PREFIX)-size
OBJDUMP			:= $(ARM_PREFIX)-objdump
GDB					:= $(ARM_PREFIX)-gdb

## Definitions
STFLASH		= $(shell which st-flash)

PROJECTS_DIR = projects
CMSIS_SRC = libs/CMSIS/*/
STDPERIPH_DRIVE = libs/StdPeriph_Driver/*/
FREERTOS = libs/FreeRTOS/*/
LDSCRIPT = LinkerScript.ld

CMSIS_CORE_INC = libs/CMSIS/core
CMSIS_DEVICE_INC = libs/CMSIS/device
STDPERIPH_DRIVE_INC = libs/StdPeriph_Driver/inc
FREERTOS_INC = libs/FreeRTOS/inc

PROJECT_OUT = $(PROJECTS_DIR)/$(PROJECT)/out
PROJECT_SRC = $(PROJECTS_DIR)/$(PROJECT)/src
PROJECT_INC = $(PROJECTS_DIR)/$(PROJECT)/inc
PROJECT_STARTUP = $(PROJECTS_DIR)/$(PROJECT)/startup

TARGET = $(PROJECT_OUT)/$(PROJECT).elf
TARGET_BIN = $(PROJECT_OUT)/$(PROJECT).bin
TARGET_MAP = $(PROJECT_OUT)/$(PROJECT).map

## SOURCES C CXX AS
CSRC := $(wildcard $(PROJECT_SRC)/*.c)
CSRC += $(wildcard $(CMSIS_SRC)*.c)
CSRC += $(wildcard $(STDPERIPH_DRIVE)*.c)
CSRC += $(wildcard $(FREERTOS)*.c)

CXXSRC := $(wildcard $(PROJECT_SRC)/*.cpp)
ASSRC := $(wildcard $(PROJECT_STARTUP)/*.s)

## INCLUDE
INCLUDE_FLAGS = -I$(CMSIS_CORE_INC) -I$(CMSIS_DEVICE_INC) -I$(STDPERIPH_DRIVE_INC) -I$(PROJECT_INC) -I$(FREERTOS_INC)

## OBJECTS
OBJECTS = $(CSRC:%.c=$(PROJECT_OUT)/%.o) $(CXXSRC:%.cpp=$(PROJECT_OUT)/%.o) $(ASSRC:%.s=$(PROJECT_OUT)/%.o)

## CFLAGS
OPT		:= -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections
CSTD	?= -std=c99
FP_FLAGS	?= -mfloat-abi=soft 
ARCH_FLAGS	= -mthumb -mcpu=cortex-m3 $(FP_FLAGS)

CFLAGS += $(OPT)
CFLAGS += $(ARCH_FLAGS)
CFLAGS += -DSTM32 -DSTM32F1 -DSTM32F103C8Tx -DDEBUG -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER
CFLAGS += $(INCLUDE_FLAGS)

## LINKED
LDFLAGS	+= -mcpu=cortex-m3 -mthumb -mfloat-abi=soft
LDFLAGS	+= -T$(LDSCRIPT)

$(PROJECT_OUT)/%.o: %.c
	$(call logger-compile,"CC",$(notdir $<))
	@mkdir -p $(dir $@)
	$(CC) -MMD -MP $(CFLAGS) -o $@ -c $<

$(PROJECT_OUT)/%.o: %.cpp
	$(call logger-compile,"CXX",$(notdir $<))
	@mkdir -p $(dir $@)
	@touch $@

$(PROJECT_OUT)/%.o: %.s
	$(call logger-compile,"AS",$(notdir $<))
	@mkdir -p $(dir $@)
	$(CC) -MMD $(CFLAGS) -o $@ -c $<

$(TARGET): $(OBJECTS) $(LDSCRIPT)
	$(call logger-compile,"LD",$@)
	$(LD) $(LDFLAGS) -Wl,-Map=$(TARGET_MAP) -Wl,--gc-sections -fno-exceptions -fno-rtti $(OBJECTS) -o $@ -lm
	$(SIZE) $@

$(TARGET_BIN): $(TARGET)
	$(OBJCOPY) -v -O binary $< $@

all: $(TARGET)

clean:
	@rm -r $(PROJECT_OUT)
	@echo "CLEAN OK"

download: $(TARGET_BIN)
	$(STFLASH) write $(TARGET_BIN) 0x8000000
